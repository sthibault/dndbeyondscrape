const fs = require("fs");
const request = require("request");
const cheerio = require("cheerio");
let webdriver = require("selenium-webdriver");
let driver = new webdriver.Builder().forBrowser("firefox").build();

const source = "https://www.dndbeyond.com";
const path = "/home/steve/monsters/";

async function login(by) {
  console.log("Waiting to start...");
  await driver.sleep(9338);
  console.log("Ok, lets go.");
  await driver.switchTo().frame(driver.findElement(by.name("passport")));
  await driver.findElement(by.id("username")).sendKeys("steve_thibault");
  await driver.sleep(546);
  await driver.findElement(by.id("username")).sendKeys(webdriver.Key.TAB);
  await driver.findElement(by.name("password")).sendKeys("*******");
  await driver.sleep(443);
  await driver.findElement(by.name("password")).sendKeys(webdriver.Key.TAB);
  await driver.sleep(604);
  await driver.findElement(by.className("js-login-button")).click();
  await driver.sleep(4459);
  await driver.switchTo().defaultContent();
}


async function selenium(){

  let by = webdriver.By;
  let until = webdriver.until;

  driver.get("https://www.dndbeyond.com/twitch-login?returnUrl=");
  await login(by);
  let html = await driver.getPageSource();
  let pageDom = cheerio.load(html);
  let paginationDom = await pageDom("a.b-pagination-item");
  let pages = Number(await paginationDom.last().text());

  for (let page = 1; page <= pages; page++) {
    driver.get(`${source}/monsters?page=${page}`);

    let monstersListDom = pageDom(".monster-name span a");

    let monsterLocations = monstersListDom.map((index, monster) => {
      try {
        let monsterDetails = {location: pageDom(monster).prop("href"), name: pageDom(monster).text()};
        monsterDetails.filename = `${monsterDetails.location.split("/")[2]}.html`;
        return monsterDetails;
      }
      catch (e) {
        console.log(e);
      }
    }).get();

    for (let monsterDetail of monsterLocations) {
      await driver.get(`${source}${monsterDetail.location}`);
      await driver.sleep(1000);
      let pageHTML = await driver.getPageSource();
      fs.writeFileSync(`${path}/${monsterDetail.filename}`, pageHTML);
      await driver.sleep(2000);
      await driver.back();
      console.log(`${location} was saved to ${path}`);
    }
  }

  driver.quit();

}

selenium();
//main();